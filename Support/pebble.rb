def pebble_app_name
  appname = "Pebble App"
  file = File.open("#{ENV['TM_PROJECT_DIRECTORY']}/appinfo.json", "rb")
  if (file.read =~ /"shortName"\s*:\s*"([^"]*)"/)
    appname = $1
  end
  file.close
  appname
end

def pebble
  File.expand_path(ENV['TM_PEBBLE'] || "~/pebble-dev/PebbleSDK-current/bin/pebble")
end